
from setuptools import setup, find_packages

from pathlib import Path

def _mltGlob(curDir, *globs):
  curDir = Path(curDir)
  outFiles = []
  for curGlob in globs:
    outFiles.extend(curDir.glob(curGlob))
  outFiles = [str(f) for f in outFiles]
  return outFiles

here = Path(__file__).parent
lines = (here/'requirements.txt').open('r').readlines()
required = []
for line in lines:
  if not line.startswith('#'):
    required.append(line.strip('\n'))


setup(
  name='davy',
  version='0.1.0',
  package_dir={'davy':'davy'},
  packages=find_packages(),
  install_requires=required,
  include_package_data=True,
  url='https://gitlab.com/ntjess_graduate/davy',
  license='',
  author='Nathan Jessurun',
  author_email='njessurun@ufl.edu',
)
