import pyqtgraph as pg
from pyqtgraph.Qt import QtCore
from utilitys import fns

from davy.app import Davy

def main(dataPath='', **kwargs):
  pg.mkQApp()
  win = Davy()
  win.show()
  # Setting minsize before showing sometimes doesn't actually resize the tree
  win.ctrl.tree.setMaximumWidth(300)
  QtCore.QTimer.singleShot(0, win.showMaximized)
  if dataPath:
    kwargs.update(dataPath=dataPath)
  win.ctrl.run_proc(**kwargs)
  pg.exec()

def main_cli():
  parser = fns.makeCli(main)
  args = parser.parse_args()
  main(**vars(args))

if __name__ == '__main__':
  main_cli()
