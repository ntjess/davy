import os.path
import pickle
from pathlib import Path

import pandas as pd
# Import triggers registering of desired parameter types
# noinspection PyUnresolvedReferences
from pyqtgraph.parametertree import Parameter
from s3a.generalutils import resize_pad
from sklearn.cluster import FeatureAgglomeration
from sklearn.decomposition import PCA, NMF, KernelPCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import NeighborhoodComponentsAnalysis
from sklearn.random_projection import GaussianRandomProjection
from sklearn.svm import SVR
from utilitys import ParamEditor, EditorPropsMixin, fns
from utilitys import RunOpts
import numpy as np

from .model import DavyModel
from .modelviz import NComponentVisualizer
from .viewer import DavyViewer

xformerMap = {
  'None': None,
  'LDA': LinearDiscriminantAnalysis(),
  'PCA': PCA(),
  'KPCA': KernelPCA(),
  'NMF': NMF(),
  'NCA': NeighborhoodComponentsAnalysis(),
  'Projection': GaussianRandomProjection(eps=0.15),
  'Agglomeration': FeatureAgglomeration()
}

class DavyCtrl(EditorPropsMixin, ParamEditor):
  model: DavyModel
  viewer: DavyViewer
  dataCache = {}

  def __init__(self, viewer, model, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.viewer = viewer
    self.model = model
    opts = dict(runOpts=RunOpts.ON_CHANGED)
    self.registerFunc(self.updateTransformer, **opts)
    self.run_proc = self.registerFunc(self.run)

    self.registerFunc(self.exploreModel)

    # Handle to NComponentVisualizer window to avoid garbage collection
    self.compVis = None
    self.selectedDataPath = None

  @fns.dynamicDocstring(lims=list(xformerMap))
  def updateTransformer(self, xformer='None', customPickle=None):
    """
    :param xformer:
      pType: list
      title: Transformer
      limits: {lims}
    :param customPickle:
      pType: filepicker
    """
    if xformer == 'Custom' and customPickle and os.path.isfile(customPickle):
      with open(customPickle, 'rb') as ifile:
        xformer = pickle.load(ifile)
    else:
      xformer = xformerMap.get(xformer)
    self.model.xformer = xformer

  def run(self, dataPath='.', refit=True, tooltipImageSize=200, imageSideLength=None, xVariable='', yVariable='',
          nameAsSymbol=False, spotSize=20):
    """
    :param dataPath:
      pType: filepicker
    :param tooltipImageSize:
    :param xVariable:
      pType: popuplineeditor
    :param yVariable:
      pType: popuplineeditor
    :param nameAsSymbol:
      Whether to use the class name as the symbol, rather than using e.g. '+', 'o', etc.
    :param spotSize: How big to make the spots
      limits: [0, None]
    """
    dataPath = self.selectedDataPath = Path(dataPath)
    if self.dataCache.get(dataPath, None) is None:
      if not dataPath.is_file():
        return
      df = pd.read_pickle(dataPath)
      self.dataCache[dataPath] = df
    df = self.dataCache[dataPath]
    shape = self.model.numImageChannels = df.iloc[0, df.columns.get_loc('image')].shape
    self.model.numImageChannels = 1 if len(shape) == 2 else shape[-1]
    data = self.imageDfToPixelsDf(df, imageSideLength)
    self.model.updateData(data, refit=refit)
    self.viewer.updateImageData(self.model.makeImage, tooltipImageSize)
    pltVarNames = []
    newLims = list(self.model.transformedData.columns)
    for var, val in zip(['x', 'y'], [xVariable, yVariable]):
      param = self.params.child('Run', f'{var}Variable')
      val = param.value()
      if val is not None and len(val) > 0 and val in newLims:
        pltVarNames.append(val)
      else:
        param.setValue('')
      param.setLimits(newLims)
    if len(pltVarNames) != 2:
      # Both must be provided to use this
      pltVarNames = None
    numericLabels, labelMap = self.model.labelParam.toNumeric(self.model.labels, returnMapping=True)
    self.viewer.updatePlotData(self.model.transformedData,
                               numericLabels=numericLabels,
                               labelToNameMap=labelMap,
                               xyColNames=pltVarNames,
                               textSymbols=nameAsSymbol,
                               size=spotSize)

  def exploreModel(self):
    """
    Explores how a model's reconstruction capabilities change with the number of used components
    """
    try:
      viz = NComponentVisualizer(self.dataCache[self.selectedDataPath], self.model.xformer)
    except KeyError:
      # Data not yet loaded
      return
    self.compVis = viz.widgetContainer()
    self.compVis.showMaximized()

  def imageDfToPixelsDf(self, df, imageSideLength=None):
    """
    From a dataframe with 'image', 'label' columns, creates individual columns for each pixel

    :param df: Dataframe with 'image' (MxNxChan ndarray) and 'label' columns
    :param imageSideLength: How big to make each side of the resized square image
    """
    if imageSideLength is None:
      formatter = resize_pad
    else:
      formatter = lambda im, size: im
    imgArr = [formatter(im, (imageSideLength, imageSideLength)).reshape(-1) for im in df['image']]
    data = pd.DataFrame(np.vstack(imgArr))
    data.columns = [f'pix{i}' for i in range(len(data.columns))]
    data['label'] = df['label']
    return data