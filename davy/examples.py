from pathlib import Path

from sklearn.datasets import load_digits, fetch_olivetti_faces
import pandas as pd
import pyqtgraph as pg
from davy.app import Davy


def faces_main():
  pg.mkQApp()
  bunch = fetch_olivetti_faces()
  data = pd.DataFrame()
  data['image'] = list(bunch['images'])
  data['label'] = [str(t) for t in bunch['target']]
  app = Davy()
  app.ctrl.dataCache[Path('visualize')] = data
  app.ctrl.run_proc(dataPath='visualize', nameAsSymbol=True)
  app.showMaximized()
  pg.exec()

if __name__ == '__main__':
  faces_main()