from pyqtgraph.Qt import QtWidgets
from utilitys.widgets import EasyWidget

from davy.control import DavyCtrl
from davy.model import DavyModel
from davy.viewer import DavyViewer

class Davy(QtWidgets.QMainWindow):
  def __init__(self, parent=None):
    super().__init__(parent)
    self.viewer = DavyViewer(self)

    self.model = DavyModel()
    self.ctrl = DavyCtrl(self.viewer, self.model)
    self.ctrl.tree.setMaximumWidth(300)

    EasyWidget.buildMainWin([self.viewer, self.ctrl.tree], useSplitter=True, layout='H', win=self)
