from __future__ import annotations
from pathlib import Path
from typing import Optional, Union, Sequence

import numpy as np
import pandas as pd
from skimage import img_as_ubyte
from sklearn.base import TransformerMixin
from sklearn.utils.validation import check_is_fitted
from utilitys import PrjParam

FilePath = Union[str, Path]

class DavyModel:
  def __init__(self):
    self.dfData = None
    self.featureData = None
    self.numImageChannels = 3
    self.transformedData = None
    self.labels = np.array([])
    self.labelParam: PrjParam | None = None
    self.xformer: TransformerMixin | None = None

  def updateData(self, inFpathOrData: Union[FilePath, pd.DataFrame]=None,
                 labelCol='label', labelParam: pd.Series=None,
                 featureCols: Sequence[str]=None,
                 refit=True):
    if inFpathOrData is None:
      data = self.dfData.copy()
    elif (isinstance(inFpathOrData, pd.DataFrame)
          or isinstance(inFpathOrData, Path)
          or isinstance(inFpathOrData, str)):
      data = self.pathToDf(inFpathOrData)
    else:
      raise Exception(f"Don't know how to handle {type(inFpathOrData)}")
    labels = data[labelCol].to_numpy()
    self.dfData = data
    if labelParam is None:
      labelParam = PrjParam(labelCol, labels[0])
    self.labelParam = labelParam

    if featureCols is None:
      featureCols = [c for c in data.columns if c != labelCol]
    data = data[featureCols]

    if self.xformer is not None:
      if refit or not check_is_fitted(self.xformer):
        xformerData = self.xformer.fit_transform(data, labels)
      else:
        # noinspection PyUnresolvedReferences
        xformerData = self.xformer.transform(data)
      transformedData = pd.DataFrame(xformerData, columns=[f'tform{ii}' for ii in range(xformerData.shape[1])])
    else:
      transformedData = data
    self.featureData = data
    self.transformedData = transformedData
    self.labels = labels
    return data, labels, labels

  @staticmethod
  def pathToDf(inFpathOrData: Union[FilePath, pd.DataFrame]=None):
    if inFpathOrData is None or isinstance(inFpathOrData, pd.DataFrame):
      data = inFpathOrData
    else:
      if inFpathOrData.is_file():
        ftype = inFpathOrData.suffix[1:]
        readFn = getattr(pd, f'read_{ftype}')
        data = readFn(inFpathOrData)
      else:
        files = inFpathOrData.glob('*.csv')
        data = pd.concat(map(pd.read_csv, files))
    return data

  def makeImage(self, rowIdx: int=None):
    """
    Determines how images are made which are mapped to each data sample. Should
    be overloaded. By default, each row of features is assumed to be a square rgb image.
    :return: List of images, where each image corresponds to one sample
    """
    imCol = self.featureData.iloc[rowIdx, :].to_numpy()
    sideLen = int(np.sqrt(len(imCol) // self.numImageChannels))
    reshaped = np.reshape(imCol, (sideLen, sideLen, -1))
    return img_as_ubyte(reshaped)