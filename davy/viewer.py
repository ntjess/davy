from functools import partial
from pathlib import Path
from typing import Union, List, Optional, Sequence, Callable

import cv2 as cv
import numpy as np
import pandas as pd
import pyqtgraph as pg
from pyqtgraph import functions as fn
from pyqtgraph.Qt import QtCore, QtGui
from matplotlib import cm
from s3a.views.fielddelegates import TextFieldDelegate
from utilitys import PrjParam

_imgCreateFnType = Callable[[int], np.ndarray]
_updateImgArgType = Union[_imgCreateFnType, Sequence[np.ndarray]]

class DavyViewer(pg.PlotWidget):
  def __init__(self, parent=None, background='default', plotItem=None, **kargs):
    super().__init__(parent, background, plotItem, **kargs)
    self.scatter = ImgScatter()
    self.variances = pg.PlotCurveItem()
    self.legend = pg.LegendItem()

    self.imgCreationFn: Optional[_imgCreateFnType] = None
    maxSize = kargs.pop('maxSize', 200)
    self.maxSize = maxSize
    self.plotItem.addItem(self.scatter)
    self.plotItem.addItem(self.variances)

  def updateImageData(self, imgCreationFn: _updateImgArgType, maxSize:int=None):
    if imgCreationFn is None and maxSize is None:
      return
    if imgCreationFn is None:
      imgCreationFn = self.imgCreationFn
    if maxSize is None:
      maxSize = self.maxSize
    self.maxSize = maxSize
    # noinspection PyTypeChecker
    if isinstance(imgCreationFn, Sequence):
      imgCreationFn = imgCreationFn.__getitem__
    self.imgCreationFn = imgCreationFn

  def _tooltipFromIdx(self, idx: int):
    buffer = QtCore.QBuffer()
    buffer.open(buffer.WriteOnly)
    img = self.imgCreationFn(idx)
    # Adapted from https://stackoverflow.com/a/34836998/9463643
    resizeRatio = self.maxSize/max(img.shape[:2])
    img = cv.resize(img, (0, 0), fx=resizeRatio, fy=resizeRatio, interpolation=cv.INTER_NEAREST)
    img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
    img = fn.makeQImage(img, transpose=False)
    img.save(buffer, "PNG", quality=100)
    encoded = bytes(buffer.data().toBase64()).decode()
    html = f'<div style="text-align:center">Row : {idx}<br/></div>' \
           f'<img src="data:image/png;base64,{encoded}">'
    return html

  def updatePlotData(self,
                     data: pd.DataFrame,
                     imgs: _updateImgArgType=None,
                     numericLabels: Sequence[int]=None,
                     labelToNameMap=None,
                     xyColNames: Sequence[str]=None,
                     textSymbols=False,
                     size=20):
    self.updateImageData(imgs)
    self.legend.clear()

    if not len(numericLabels):
      self.scatter.clear()
      return
    if labelToNameMap is None:
      uniques = np.unique(numericLabels)
      labelToNameMap = pd.Series(uniques, uniques)
    if textSymbols:

      symbols_scales = np.vstack([TextFieldDelegate.makeTextSymbol(name or '<>', returnScale=True)
                        for name in labelToNameMap.values])
      symbols = symbols_scales[:,0]
      # Scale by 12pt font
      fontToSizeMultiplier = size/12
      size = pd.Series(index=labelToNameMap.index, data=fontToSizeMultiplier/symbols_scales[:,1])[numericLabels].values
    else:
      symbols = np.array(['+', 't', 'o', 'x', 'd', 's'])
      numLbls = len(labelToNameMap)
      symbols = np.tile(symbols, int(np.ceil(numLbls / len(symbols))))[:numLbls]
    symbols = pd.Series(symbols, index=labelToNameMap.index)
    colors = cm.get_cmap('Set3')

    rescaled = np.asarray(labelToNameMap.index, dtype=float) - np.min(labelToNameMap.index)
    rescaled /= np.max(rescaled)
    rgbLut = [pg.mkColor(*c) for c in (colors(rescaled)[:, :-1] * 255).astype('uint8')]
    rgbLut = pd.Series(rgbLut, index=labelToNameMap.index)


    if xyColNames is None:
      xyColNames = data.columns[:2]
    data = data[xyColNames[:2]].to_numpy()
    unevaluatedData = [partial(self._tooltipFromIdx, ii) for ii in range(len(data))]
    self.scatter.setData(*data.T, data=unevaluatedData,
                         symbol=symbols[numericLabels].values, brush=rgbLut[numericLabels].values, size=size)
    self.sceneObj.removeItem(self.legend)
    newLegend = pg.LegendItem()
    self.legend = newLegend
    self.legend.setParentItem(self.plotItem.getViewBox())
    if textSymbols:
      self.legend.hide()
    else:
      self.legend.show()
      for ii, name in enumerate(labelToNameMap.values):
        self.legend.addItem(
          pg.ScatterPlotItem([], [], brush=rgbLut.iloc[ii], symbol=symbols.iloc[ii], size=self.scatter.opts['size']), str(name))
    self.getViewBox().autoRange()

FilePath = Union[Path, str]


class ImgScatter(pg.ScatterPlotItem):
  def __init__(self, *args, **kargs):
    super().__init__(*args, **kargs, pxMode=True, size=20)

  def hoverEvent(self, ev: QtGui.QHoverEvent):
    if not hasattr(ev, '_scenePos'): return
    pts = self.pointsAt(ev.pos())
    if len(pts) > 0:
      pt = pts[-1]
      data = pt.data()
      if isinstance(data, Callable):
        # Data hasn't been evaluated yet
        try:
          data = data()
          pt.setData(data)
        except Exception as ex:
          data = None
    else:
      data = None
    self.setToolTip(data)
