# Data + Feature Visualization Tool
## Installation
Installation can be accomplished using pip:
```bash
git clone https://gitlab.com/ntjess/davy
pip install -e ./davy
```

# Running the App
Davy is able to analyze any dataframe that contains an `image` column (of the N-channel image to display on hovering) and a `label` column (string-name of the class each row belongs to)

To display data where the image itself is not the feature set on which to train a classifier, you must subclass the `DavyModel` class and re-implement methods for generating an image and extracting features.
